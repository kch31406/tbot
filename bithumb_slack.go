package main

import (
	"fmt"
	"log"
	"github.com/slack-go/slack"
	"github.com/thrasher-corp/gocryptotrader/config"
)

func slack_push_msg(mTitle string, sTitle string, Msg string) {

	cfg := config.GetConfig()
	err := cfg.LoadConfig("./config/config.json", true)
	if err != nil {
		log.Fatal("Bithumb load config error", err)
	}
	// Define slack configuration
	sConfig := cfg.GetCommunicationsConfig()
	api := slack.New(sConfig.SlackConfig.VerificationToken)
	attachment := slack.Attachment{
		// Uncomment the following part to send a field too
		Fields: []slack.AttachmentField{
			slack.AttachmentField{
				Title: sTitle,
				Value: Msg,
			},
		},
	}

	channelID, timestamp, err := api.PostMessage(
		sConfig.SlackConfig.TargetChannel,
		slack.MsgOptionText(mTitle, false),
		slack.MsgOptionAttachments(attachment),
		slack.MsgOptionAsUser(false),
		//slack.MsgOptionAsUser(true), // Add this if you want that the bot would post message as a user, otherwise it will send response using the default slackbot
	)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Printf("Message successfully sent to channel %s at %s", channelID, timestamp)
}
