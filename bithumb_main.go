package main

import (
	"fmt"
	"log"
	"sync"
	"time"
	"strings"
	//"os"
	//"math"
	"bithumb/libbithumb"
	"github.com/thrasher-corp/gocryptotrader/config"
	//"github.com/thrasher-corp/gocryptotrader/exchanges/bithumb"
	//"github.com/thrasher-corp/gocryptotrader/portfolio/withdraw
)

//03394c5dde26a7859e494ba325044c12	be299150d50c0e04cd004f35b103e63e <-
//6ebc8ef8f8591124c53b0ba11286e7d8  4b5dc55908f3648dc2abde113724bc26
/*
const (
	apiKey                  = "03394c5dde26a7859e494ba325044c12"
	apiSecret               = "be299150d50c0e04cd004f35b103e63e"
	canManipulateRealOrders = true
	bithumbCurrency         = "ada"
	Limit                   = 0.9
)
*/

var b *bithumb.Bithumb
var baldb *Db
var orderdb *Db
var configdb *Db

func main() {
	slack_push_msg("Bot-Info", "start", time.Now().Format("2006-01-02 15:04:05"))

	cfg := config.GetConfig()
	err := cfg.LoadConfig("./config/config.json", true)
	if err != nil {
		log.Fatal("Bithumb load config error", err)
	}
	bitConfig, err := cfg.GetExchangeConfig("Bithumb")
	if err != nil {
		log.Fatal("Bithumb Setup() init error")
	}
	
	b = new(bithumb.Bithumb)
	b.SetDefaults()
	
        bithumbCurrency := strings.ToLower(bitConfig.API.Credentials.ClientID)
/*
	bitConfig.API.AuthenticatedSupport = true
	bitConfig.API.Credentials.Key = apiKey
	bitConfig.API.Credentials.Secret = apiSecret
*/
	err = b.Setup(bitConfig)
	if err != nil {
		log.Fatal("Bithumb setup error", err)
	}

	baldb = NewDB("balance")
	orderdb = NewDB("orders")
	configdb = NewDB("config")
/*
	data, err := stop_n_go(b, bithumbCurrency)
	if err != nil {
		log.Fatal("Bithumb getBalanceInfo error", err)
		return
	}
	baldb.write(bithumbCurrency, data)
*/	
	cond, err := setCondition(bithumbCurrency)
	if err != nil {
		fmt.Printf("setCondition fail")
		return
	}

	var wait sync.WaitGroup
	watch := NewWatch(b, bithumbCurrency)

	///////////////TEST CODE///////////////////////*
/*
	for {
		//detail, _ := b.GetOrderDetails("C0150000000090849040", "ADA")
		//detail := watch.getCurrentPrice()
		/*
			if err != nil {
				fmt.Println("Bithumb GetOrderDetails() error", err)
			}
		*/
		//s, _ := strconv.ParseFloat(detail, 32)
				
				/*
		//fmt.Println(detail)
		//안전하게 절삭하고 시작할가
		fmt.Println("===========================================================")
		fmt.Println("BALANCE=====================")
		a, b := watch.getBalance()
		c := math.Floor(b)
		//c := b
		fmt.Println("xcoin ", a)
		fmt.Println("krw ", c)
		fmt.Println("ACCOUNT=====================")
		
		price := watch.getCurrentPrice()
		fmt.Printf("current price %f\n", price)
		fmt.Println("minium count ", GetMiniumOrderCount(price))
		fmt.Println("fee" , watch.getTradeFee())
		amout := c/41130
		fee := watch.calculateTradingFee(price, amout)
		fmt.Println("trading fee ", fee)
		//f := math.Floor(c-fee)
		f := c-fee
		fmt.Println("maxiump ", f/41130)
		fmt.Println("===========================================================")

		time.Sleep(time.Second * 1)
	}

	*/
	////////////////////////////////////////////////

	wait.Add(1)

	go watch.run(cond)

	wait.Wait()

	slack_push_msg("Bot-Info", "shutdown", time.Now().Format("2006-01-02 15:04:05"))
}

func setCondition(c string) (*Condition, error) {
	data := Detail{}
	err := configdb.read("condition", &data)
	if err != nil {
		fmt.Println("Init Condition() error", err)
		return nil, err
	}
	cond := NewCondition(c, &data)

	if strings.ToLower(data.InitPosition) == "buy" {
	  cond.setPosition(BIDPos)
	} else {
	  cond.setPosition(ASKPos)	
	}
	if strings.ToLower(data.Strategy) == "full" {
	  cond.setStrategy(FULLTrade)
	} else {
	  cond.setStrategy(UNITTrade)	
	}

	return cond, nil
}

/*
func stop_n_go(b *bithumb.Bithumb, c string) (bithumb.FullBalance, error) {

	tm := time.Now().Format("2006-01-02 15:04:05")
	curr := c
	data, err := b.GetAccountBalance(curr)
	if err != nil {
		fmt.Println("Bithumb GetAccountBalance() error", err)
		return data, err
	}

	fmt.Println("in_misu")
	for key, val := range data.Misu {
		fmt.Println(key, val)
	}
	fmt.Println("total")
	for key, val := range data.Total {
		fmt.Println(key, val)
	}

	fmt.Println("xcoin")
	for key, val := range data.Xcoin {
		fmt.Println(key, val)
	}
	fmt.Println("in_use")
	for key, val := range data.InUse {
		fmt.Println(key, val)
		slack_push_msg(tm, "in_use_"+key, fmt.Sprintf("%f", val))
	}

	fmt.Println("available")
	for key, val := range data.Available {
		fmt.Println(key, val)
		slack_push_msg(tm, "available_"+key, fmt.Sprintf("%f", val))

	}

	// /return data, errors.New("잔액부족")

	return data, nil
}
*/
