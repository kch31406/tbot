package main

//03394c5dde26a7859e494ba325044c12	be299150d50c0e04cd004f35b103e63e

import (
	"fmt"
	"sync"
	"time"
	"os"
	"math"
	"strings"
	"bithumb/libbithumb"
)

const (
	BIDPos      = 0
	ASKPos      = 1
	UNITTrade  = 2
	FULLTrade   = 3
)

type Detail struct {
	AVGPrice    float64
	InitCnt     float64
	Strategy    string 
	Stgy    int32
	InitPosition string
	BidmaxLimit float64
	BidminLimit float64
	AskmaxLimit float64
	AskminLimit float64
	MiniumOrderCount float64
	MiniumOrderPrice float64
}

type PriceBox struct {
	current_price float64
	units	      float64  // 0:full, 0!= units
	min           float64
	max           float64
}

type Condition struct {
	currency string
	Position int32
	TradingFee  float64
	Data     *Detail
	BidWatch PriceBox
	AskWatch PriceBox
	m        *sync.Mutex
}

func NewCondition(c string, data *Detail) *Condition {
	return &Condition{currency: c,
		Position: ASKPos, // bid "0"(buy), ask "1"(sell)
		Data:     data,
		BidWatch: PriceBox{},
		AskWatch: PriceBox{},
		m:        &sync.Mutex{}}
}

func (c *Condition) updatePirce(current_price float64) {
	fmt.Printf("current pricde %f\n", current_price)

	precision := GetMiniumOrderPrice(current_price)
	fmt.Printf("precision %f\n", precision)

	c.BidWatch.current_price = current_price
	c.BidWatch.max = current_price * c.Data.BidmaxLimit
	c.BidWatch.max = math.Floor((c.BidWatch.max / precision)) * precision
	c.BidWatch.min = current_price * c.Data.BidminLimit
	c.BidWatch.min = math.Floor((c.BidWatch.min / precision)) * precision

	fmt.Printf("bid max %f(%f)\n", c.BidWatch.max, c.Data.BidmaxLimit)
	fmt.Printf("bid min %f(%f)\n", c.BidWatch.min, c.Data.BidminLimit)

	c.AskWatch.current_price = current_price
	c.AskWatch.max = current_price * c.Data.AskmaxLimit
	c.AskWatch.max = math.Floor((c.AskWatch.max / precision)) * precision
	c.AskWatch.min= current_price * c.Data.AskminLimit
	c.AskWatch.min = math.Floor((c.AskWatch.min / precision)) * precision

	fmt.Printf("ask max %f(%f)\n", c.AskWatch.max, c.Data.AskmaxLimit)
	fmt.Printf("ask min %f(%f)\n", c.AskWatch.min,  c.Data.AskminLimit)
}

func (c *Condition) getPosition() int32 {
	return c.Position 
}

func (c *Condition) setPosition(sPosition int32) int32 {
	c.Position = sPosition
	return c.Position 
}

func (c *Condition) getStrategy() int32 {
	return c.Data.Stgy
}

func (c *Condition) setStrategy(s int32) int32 {
	c.Data.Stgy = s
	return c.Data.Stgy
}

func (c *Condition) getPositionString() string {
	if c.Position == BIDPos {
		return "bid"
	} else {
		return "ask"
	}
	return ""
}

func (c *Condition) read(pos int32) (float64, float64) {
	if pos == BIDPos {
		return c.BidWatch.min, c.BidWatch.max
	} else {
		return c.AskWatch.min, c.AskWatch.max
	}
	return -1, -1
}

type Watch struct {
	b         *bithumb.Bithumb
	currency  string
	fee 	  float64
}

func NewWatch(bt *bithumb.Bithumb, c string) *Watch {
	return &Watch{b: bt, currency: c, fee: -1}
}

func (w *Watch) preProcessing() {
	var id string
	ids, _ := orderdb.readAll()
	if len(ids) > 0 {
		for _, val := range ids {
			id = val[1 : len(val)-1]
			detail, err := w.b.GetOrderDetails(id, w.currency)
			if err != nil {
				fmt.Println("Bithumb GetOrderDetails() error", err)
				continue
			} else {
				fmt.Println(detail.Data.Type)
				fmt.Println(id, w.currency)
				_, err = w.b.CancelTrade(detail.Data.Type, id, w.currency)
				if err != nil {
					fmt.Println("Bithumb CancelTrade() error", err)
					continue
				}
				orderdb.delete(id)
				slack_push_msg("pre-Cancel", id, time.Now().Format("2006-01-02 15:04:05"))
			}
		}
	}
}

func (w *Watch) sellPosition(cond *Condition) string {
	var units float64
	var price float64
	var xcoin float64

	price = cond.AskWatch.min
	if cond.getStrategy() == FULLTrade {
		xcoin, _ = w.getBalance()
		units = xcoin
	} else {
		units = cond.Data.InitCnt
	}
	units = ChgNumbertoBithumb(units)
	
	if units < GetMiniumOrderCount(price) {
		fmt.Println("GetMiniumOrderCount")
		os.Exit(3)
	}

	//os.Exit(3)
	data, err := w.b.StopLimitTrade(w.currency, "ask", units, price, price)
	if err != nil {
		fmt.Println("Bithumb StopLimitTrade() error", err)
		os.Exit(3)
	}
	orderdb.write(data.OrderID, data.OrderID)
	
	msg := fmt.Sprintf("%s \nprice: %f \nunit: %f \n", cond.currency, cond.AskWatch.min, 
			ChgNumbertoBithumb(units))
	slack_push_msg("Init-Condition(SELL)", msg, time.Now().Format("2006-01-02 15:04:05"))

	_, err = w.b.GetOrderDetails(data.OrderID, w.currency)
	if err != nil {
		fmt.Println("Bithumb GetOrderDetails() error", err)
		os.Exit(3)
	}

	return data.OrderID
}

func (w *Watch) loopOrderComplete(orderID string) {
	for {
	   rst := w.switchCheck(orderID)
	   if (rst != -1) {
	   	return
	   }
	
	   time.Sleep(time.Second * 1)
	}
}

func (w *Watch) buyComplete(cond *Condition) string {
	var units float64
	var price float64
	var krw float64
	var fee float64

	price = cond.BidWatch.current_price
	if cond.getStrategy() == FULLTrade {
		_, krw = w.getBalance()
		krw = math.Floor(krw)
		fmt.Println(krw)
		units = krw / price
		fee = w.calculateTradingFee (price, units)
		krw = krw - fee
		units = krw / price
	} else {
		units = cond.Data.InitCnt
	}
	units = ChgNumbertoBithumb(units)
	//fmt.Println(krw, ChgNumbertoBithumb(price), fee)
	//fmt.Printf("%f\n", units)
	//fmt.Printf("%f\n", price)
	//fmt.Printf("krw %f unit %f fee %f\n", ChgNumbertoBithumb(krw), ChgNumbertoBithumb(units), ChgNumbertoBithumb(fee))
	//os.Exit(3)
	if units < GetMiniumOrderCount(price) {
		fmt.Println("GetMiniumOrderCount")
		os.Exit(3)
	}

	data, err := w.b.PlaceTrade(w.currency, "bid", units, price)
	if err != nil {
		fmt.Println("Bithumb PlaceTrade() error", err)
		os.Exit(3)
	}

	orderdb.write(data.OrderID, data.OrderID)
	w.loopOrderComplete(data.OrderID)

	msg := fmt.Sprintf("%s \nprice: %f \nunit: %f \n", cond.currency, price, ChgNumbertoBithumb(units))
	slack_push_msg("Init-Condition(BUY)", msg, time.Now().Format("2006-01-02 15:04:05"))

	return data.OrderID
}

func (w *Watch) updatePosition(orderID string, cond *Condition) string {
	var watchPrice float64
	var units float64
	var transType string
	
	if orderID != "" {
		_, err := w.b.CancelTrade(cond.getPositionString(), orderID, w.currency)
		if err != nil {
			fmt.Println("Bithumb CancelTrade() error", err)
			return ""
		}
		orderdb.delete(orderID)
		slack_push_msg("upd-Cancel", orderID, time.Now().Format("2006-01-02 15:04:05"))
	}

	transType, watchPrice, units = w.GetAmountPrice(cond)

	data, err := w.b.StopLimitTrade(w.currency, transType, units, watchPrice, watchPrice)
	if err != nil {
		fmt.Println("Bithumb StopLimitTrade() error", err)
		return ""
	}
	orderdb.write(data.OrderID, data.OrderID)
	slack_push_msg("upd-position", data.OrderID, time.Now().Format("2006-01-02 15:04:05"))
	msg := fmt.Sprintf("%s \nid: %s \nwatch-price: %f \nunit: %f \ntype: %s\n", cond.currency, orderID,
		watchPrice, units, transType)
	slack_push_msg("upd-Condition", msg, time.Now().Format("2006-01-02 15:04:05"))

	return data.OrderID
}

/*
구매전 pending check
Bithumb request type: POST
Bithumb request body: endpoint=%2Finfo%2Forder_detail&order_currency=ADA&order_id=C0150000000089901955&payment_currency=KRW
HTTP status: 200 OK, Code: 200
Bithumb raw response: {"status":"0000","data":{"order_date":"1613228941041154","type":"ask","order_status":"Pending","order_currency":"ADA","payment_currency":"KRW","watch_price":"501","order_price":"501","order_qty":"98.9","cancel_date":"","cancel_type":"","contract":[]}}
{0000 {1613228941041154 ask Pending ADA KRW 501 501 98.9   []} }
*/
func (w *Watch) run(cond *Condition) {
	//base price is current price
	var orderID string
	var box_min, box_max, cPrice float64

	w.preProcessing()

	cPrice = w.getCurrentPrice()
	if cPrice < 0 {
		return
	}
	// price min, max update first
	cond.updatePirce(cPrice)
	//os.Exit(3)
	// buy, first (market price ? target price
	if cond.getPosition() == BIDPos {
		_ = w.buyComplete(cond)
		cond.setPosition(ASKPos)
	}	

	orderID = w.sellPosition(cond)

	box_min, box_max = cond.read(ASKPos)
	for {
		cPrice = w.getCurrentPrice()
		if cPrice < 0 {
			return
		}

		if cond.getPosition() == ASKPos {
			if box_max <= cPrice {
				cond.updatePirce(cPrice)
				box_min, box_max = cond.read(ASKPos)
				orderID = w.updatePosition(orderID, cond)
			}
		} else {
			if box_min >= cPrice {
				cond.updatePirce(cPrice)
				box_min, box_max = cond.read(BIDPos)
				orderID = w.updatePosition(orderID, cond)
			}
		}

		// when it switch over, perform set position
		//fmt.Println("orderid,type,status check switch over")
		rst := w.switchCheck(orderID)
		if rst == BIDPos {
			cond.setPosition(BIDPos)
			fmt.Println("ASKPos->BIDPos switch")
			slack_push_msg("ASKPos->BIDPos switch", orderID, time.Now().Format("2006-01-02 15:04:05"))
			orderID = w.updatePosition("", cond)
		} else if rst == ASKPos {
			cond.setPosition(ASKPos)
			fmt.Println("BIDPos->ASKPos switch")
			slack_push_msg("BIDPos->ASKPos switch", orderID, time.Now().Format("2006-01-02 15:04:05"))
			orderID = w.updatePosition("", cond)
		}

		time.Sleep(time.Second * 1)
	}
}

/*
Bithumb raw response: {"status":"0000","data":{"order_date":"1613228941041154","type":"ask","order_status":"Pending","order_currency":"ADA","payment_currency":"KRW","watch_price":"501","order_price":"501","order_qty":"98.9","cancel_date":"","cancel_type":"","contract":[]}}
{0000 {1613228941041154 ask Pending ADA KRW 501 501 98.9   []} }

Bithumb request body: endpoint=%2Finfo%2Forder_detail&order_currency=ADA&order_id=C0150000000090476242&payment_currency=KRW
Bithumb GetOrderDetails() error Bithumb unsuccessful HTTP status code: 400 raw response: {"status":"5100","message":"Bad Request.(Request Time:reqTime1613314367723\/nowTime1613314387891)"}

(base) [parallels@platformhost bithumb]$ date
Mon Feb 15 00:01:15 JST 2021
(base) [parallels@platformhost bithumb]$ ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
*/
func (w *Watch) switchCheck(orderID string) int32 {
	if orderID == "" {
		return -1
	}
	detail, err := w.b.GetOrderDetails(orderID, w.currency)
	if err != nil {
		fmt.Println("Bithumb GetOrderDetails() error", err)
		return -1
	}
	if detail.Data.OrderStatus == "Completed" {
		orderdb.delete(orderID)
		if detail.Data.Type == "ask" {
			return BIDPos
		} else {
			return ASKPos
		}
	}
	return -1
}

func (w *Watch) getBalance() (coin float64, krw float64) {
	data, err := b.GetAccountBalance(w.currency)
	if err != nil {
		fmt.Println("Bithumb GetAccountBalance() error", err)
		return -1, -1 
	}
	fmt.Println(data.Total["krw"])
	return data.Total[strings.ToLower(w.currency)], data.Total["krw"]
}

func (w *Watch) getTradeFee() float64 {
	data, err := b.GetAccountInformation(w.currency, "")
	if err != nil {
		fmt.Println("Bithumb GetAccountBalance() error", err)
		return -1
	}

	return data.Data.TradeFee * 2
}

func (w *Watch) calculateTradingFee (price, amount float64) float64 {
	var fee float64 = w.fee
	if fee < 0 {
	   fee = w.getTradeFee()	
	   w.fee = fee
	}
	fmt.Println("fee ", fee)
	return fee * price * amount
}

func (w *Watch) getCurrentPrice() float64 {
	data, err := w.b.GetTicker(w.currency)
	if err != nil {
		fmt.Println("Bithumb GetTicker() error", err)
		return -1
	}
	return data.ClosingPrice
}
