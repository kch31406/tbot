CC=go
CR=run
CB=build

SRCS=	\
bithumb_main.go	\
bithumb_db.go	\
bithumb_loop.go	\
bithumb_slack.go	\
bithumb_util.go	\


EXENAME=bot-v1
LIBNAME=coremanager.so

EXEDIR=$(shell pwd)

CFLAGS=-o 
BFLAGS=-buildmode=c-shared

pre-run=$(CC) $(CR) $(SRCS)
pre-build=$(CC) $(CB) $(CFLAGS) $(EXENAME) $(SRCS)
shared-build=$(CC) $(CB) $(CFLAGS) $(LIBNAME) $(BFLAGS) $(SRCS)
run:
	$(pre-run)	 

build:
	$(pre-build)

shared:
	$(shared-build)

clean:
	rm $(EXEDIR)/$(EXENAME)	

delete:
	rm $(EXEDIR)/$(LIBNAME)

####lib path add#######GOPATH#####GOROOT#######
