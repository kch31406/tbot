package main

import (
	"math"
	"fmt"
	"os"
)
/*
func main() {
	

	var total float64 = 15979
	var price float64 = 600
	var amount float64 = total/price
	fmt.Println("amout", amount)
	fmt.Println("fee", 0.0025 * amount * 600)
/*
주문푀소가 1개 이상일때
주문가능 15,979.95 <- 소수점 뒷 자리 삭제
수무수량 주문가능/지정가
수수료 = 0.0025 * 주문수량 * 지정가
실제 주만가능  주문가능 - 수수료
*/

/*

}
*/

//호가 
func GetMiniumOrderPrice(reservedPrice float64) float64 {
	if (reservedPrice < 1) {
		return 0.0001
	} else if (reservedPrice >= 1 && 
	reservedPrice < 10) {
		return 0.001
	} else if (reservedPrice >= 10 && 
	reservedPrice < 100) {
		return 0.01
	} else if (reservedPrice >= 100 && 
	reservedPrice < 1000) {
		return 0.1
	} else if (reservedPrice >= 1000 && 
	reservedPrice < 5000) {
		return 1
	} else if (reservedPrice >= 5000 && 
	reservedPrice < 10000) {
		return 5
	} else if (reservedPrice >= 10000 && 
	reservedPrice < 50000) {
		return 10
	} else if (reservedPrice >= 50000 && 
	reservedPrice < 100000) {
		return 50
	} else if (reservedPrice >= 100000 && 
	reservedPrice < 500000) {
		return 100
	} else if (reservedPrice >= 500000 && 
	reservedPrice < 1000000) {
		return 500
	} else {
		return 1000
	}

	return -1
}

//최소주문량
// 0.xxxx 자리로 주문수량을 관리하고최소주문량보다 많아야 함.
func GetMiniumOrderCount(reservedPrice float64) float64 {
	if (reservedPrice < 100) {
		return 10
	} else if (reservedPrice >= 100 && 
	reservedPrice < 1000) {
		return 1
	} else if (reservedPrice >= 1000 && 
	reservedPrice < 10000) {      
		return 0.1
	} else if (reservedPrice >= 10000 && 
	reservedPrice < 100000) {
		return 0.01
	} else if (reservedPrice >= 100000 && 
	reservedPrice < 1000000) {
		return 0.001
	} else {
		return 0.0001
	}

	return -1
}

// .0001
func ChgNumbertoBithumb (p float64) float64 {
	return math.Floor(p * 10000) / 10000
}

func (w *Watch) GetAmountPrice (cond *Condition) (string, float64, float64) {
	var Type string
	var watchPrice float64
	var krw float64
	var units float64
	var fee float64

	if cond.getPosition() == BIDPos {
		watchPrice = cond.BidWatch.max
		Type = "bid"
		if cond.getStrategy() == FULLTrade {
			_, krw = w.getBalance()
			krw = math.Floor(krw)
			units = krw / watchPrice
			fee = w.calculateTradingFee (watchPrice, units)
			krw = krw - fee
			units = krw / watchPrice
		} else {
			units = cond.Data.InitCnt
		}
	} else {
		watchPrice = cond.AskWatch.min
		Type = "ask"
		if cond.getStrategy() == FULLTrade {
			units, _ = w.getBalance()
		} else {
			units = cond.Data.InitCnt
		}
	}

	fmt.Printf("krw %f unit %f fee %f\n", 
	ChgNumbertoBithumb(watchPrice), ChgNumbertoBithumb(units), ChgNumbertoBithumb(fee))
	if units < GetMiniumOrderCount(watchPrice) {
		fmt.Println("GetMiniumOrderCount")
		os.Exit(3)
	}

	return Type, ChgNumbertoBithumb(watchPrice), ChgNumbertoBithumb(units)
}
