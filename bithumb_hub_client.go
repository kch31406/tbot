package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)


// Client is a middleman between the connection and the hub.
type Client struct {
	hub *Hub
	// Buffered channel of outbound messages.
	send chan interface
	recv chan interface
}

// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
	}()

	for {

		c.hub.broadcast <- message
	}
}

// executing all writes from this goroutine.
func (c *Client) writePump() {
	defer func() {
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			//fmt.Printf("send ==> %s\n", message)

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveClient(hub *Hub, w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := new(Client)
	client.hub = hub
	client.conn = conn
	client.send = make(chan []byte, 2048)

	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}

func (c *Client) procPrivate() {
	defer func() {
		c.conn.Close()
	}()

	fmt.Printf("procPrivate\n")
	ch := make(chan []byte)

	fmt.Printf("go ticker start\n")
	go func(c *Client) {
		ticker := time.NewTicker(pingPeriod)
		defer func() {
			ticker.Stop()
			c.conn.Close()
		}()
		for {
			fmt.Printf("go ticker\n")
			select {
			case <-ticker.C:
				c.conn.SetWriteDeadline(time.Now().Add(writeWait))
				if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
					return
				}
			}
		}
	}(c)

	fmt.Printf("anoy start\n")
	go func(ch <-chan []byte, c *Client) {

		for {
			inputBytes := <-ch
			fmt.Printf("anony fuck : %s", inputBytes)
			var params ReqParameters
			err := json.Unmarshal(inputBytes, &params)
			if err != nil {
				panic(err)
			}

			p := PrivateAPI{}
			respbytes := p.SetType(params.Command, params.Data[0].Currency, params.Data[0].Payment).SetKey(params.Data[0].APIKey, params.Data[0].SecretKey).Req()

			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				fmt.Printf("connext : %v\n", err)
				return
			}
			w.Write(respbytes)

			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(respbytes)
			}

			fmt.Printf("send ==> %s\n", respbytes)

			if err := w.Close(); err != nil {
				fmt.Printf("conclose : %v\n", err)
				return
			}
		}

	}(ch, c)

	fmt.Printf("Read start\n")
	//params := new(ReqParameters)
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	fmt.Printf("entrice\n")
	for {
		fmt.Printf("begin\n")
		//err := c.conn.ReadJSON(params)
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			fmt.Printf("break %v\n", err)
			break
		}

		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		fmt.Printf("%s\n", message)
		ch <- message
		/*
			jsonBytes, err := json.Marshal(GU)
			if err != nil {
				panic(err)
			}
			fmt.Printf("%s\n", jsonBytes)
			ch <- jsonBytes
		*/
	}
}

func servePrivateClient(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := new(Client)
	client.conn = conn

	go client.procPrivate()
}
