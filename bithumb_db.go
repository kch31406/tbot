package main

import (
	"fmt"

	scribble "github.com/nanobox-io/golang-scribble"
)

type Db struct {
	dbname string
	dv     *scribble.Driver
}

func NewDB(c string) *Db {

	dir := "./"

	db, err := scribble.New(dir, nil)
	if err != nil {
		fmt.Println("Error", err)
		return nil
	}

	return &Db{dbname: c, dv: db}
}

func (d *Db) write(id string, v interface{}) {
	d.dv.Write(d.dbname, id, v)
}

func (d *Db) readAll() ([]string, error) {
	return d.dv.ReadAll(d.dbname)
}

func (d *Db) read(id string, v interface{}) error {
	return d.dv.Read(d.dbname, id, &v)
}

func (d *Db) delete(id string) error {
	return d.dv.Delete(d.dbname, id)
}

func (d *Db) deleteAll() error {
	return d.dv.Delete(d.dbname, "")
}
