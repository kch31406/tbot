package main

import (
	"context"
	"fmt"
	"reflect"
	"time"

	"github.com/qmuntal/stateless"
)

const (
	triggerSellPosition    = "SellPosition"
	triggerInitMonitoring  = "InitMonitoring"
	triggerCallMonitoring  = "CallMonitoring"
	triggerChgSellPosition = "ChgSellPosition"
	triggerBuyPosition     = "BuyPosition"
	triggerChgBuyPosition  = "ChgBuyPosition"
	triggerDestroy         = "Destroy"
)

const (
	stateInitBuy        = "InitBuy"
	stateSellPosition   = "SellPosition"
	stateMointoring     = "Monitoring"
	stateInitMointoring = "InitMonitoring"
	stateBuyPosition    = "BuyPosition"
	stateDestroy        = "Destroy"
)

var deal *stateless.StateMachine

func SAMPLE() {

	deal = stateless.NewStateMachine(stateInitBuy)
	deal.SetTriggerParameters(triggerSellPosition, reflect.TypeOf(0)) //<-define triggering pararmeter
	deal.SetTriggerParameters(triggerInitMonitoring, reflect.TypeOf(0), reflect.TypeOf(0))
	deal.SetTriggerParameters(triggerCallMonitoring, reflect.TypeOf(0), reflect.TypeOf(0))
	deal.SetTriggerParameters(triggerChgSellPosition, reflect.TypeOf(0))
	deal.SetTriggerParameters(triggerBuyPosition, reflect.TypeOf(0))
	deal.SetTriggerParameters(triggerChgBuyPosition, reflect.TypeOf(0))
	deal.SetTriggerParameters(triggerDestroy, reflect.TypeOf(0))

	deal.Configure(stateInitBuy).Permit(triggerSellPosition, stateSellPosition)

	deal.Configure(stateSellPosition).PermitReentry(stateSellPosition).OnEntryFrom(triggerSellPosition,
		func(_ context.Context, args ...interface{}) error {
			fmt.Printf("State is %s\n", deal.MustState())
			sellposition(args[0].(int))
			return nil
		}).Permit(triggerInitMonitoring, stateInitMointoring).
		Permit(triggerDestroy, stateDestroy)

	deal.Configure(stateBuyPosition).OnEntryFrom(triggerBuyPosition,
		func(_ context.Context, args ...interface{}) error {
			fmt.Printf("State is %s\n", deal.MustState())
			buyposition(args[0].(int))
			return nil
		}).Permit(triggerInitMonitoring, stateInitMointoring).
		Permit(triggerDestroy, stateDestroy)

	deal.Configure(stateInitMointoring).OnEntryFrom(triggerInitMonitoring,
		func(_ context.Context, args ...interface{}) error {
			fmt.Printf("State is %s\n", deal.MustState())
			initemonitoring(args[0].(int), args[0].(int))
			return nil
		}).Permit(triggerCallMonitoring, stateMointoring)

	deal.Configure(stateMointoring).OnEntryFrom(triggerCallMonitoring,
		func(_ context.Context, args ...interface{}) error {
			fmt.Printf("State is %s\n", deal.MustState())
			pricemonitoring(args[0].(int), args[0].(int))
			return nil
		}).
		Permit(triggerBuyPosition, stateBuyPosition).
		Permit(triggerSellPosition, stateSellPosition).
		Permit(triggerChgSellPosition, stateSellPosition).
		Permit(triggerChgBuyPosition, stateBuyPosition).
		Permit(triggerDestroy, stateDestroy)

	deal.Configure(stateDestroy).OnEntryFrom(triggerDestroy,
		func(_ context.Context, args ...interface{}) error {
			fmt.Printf("State is %s\n", deal.MustState())
			terminateapp(args[0].(int))
			return nil
		})

	//fmt.Println(deal.ToGraph())

	deal.Fire(triggerSellPosition, 1)
	/*
		deal.Fire(triggerSellPosition, 1)
		deal.Fire(triggerCallMonitoring, 1000, 0)
		deal.Fire(triggerChgSellPosition, 2)
		deal.Fire(triggerCallMonitoring, 1000, 0)
		deal.Fire(triggerBuyPosition, 3)
		deal.Fire(triggerCallMonitoring, 800, 1)
		deal.Fire(triggerChgSellPosition, 4)
		deal.Fire(triggerDestroy, 700)
	*/

	time.Sleep(time.Second * 10)
}

func sellposition(cur_price int) {
	//fmt.Printf("sell_price %d posion %d\n", cur_price, cur_price)
	//time.Sleep(time.Second * 1)
    deal.Fire(triggerInitMonitoring, 1000, 0)
}

func buyposition(cur_price int) {
	//fmt.Printf("buy_price %d posion %d\n", cur_price, cur_price)
	//time.Sleep(time.Second * 1)
}

func pricemonitoring(watch_price int, status int) {
	//tmp := "bid"
	//	if status == 0 {
	//		tmp = "ask"
	//	}
	//fmt.Printf("watch_price %d %s\n", watch_price, tmp)
	//time.Sleep(time.Second * 1)
	deal.Fire(triggerChgSellPosition, 2)
	//deal.Fire(triggerDestroy, 700)

}

func initemonitoring(watch_price int, status int) {
	deal.Fire(triggerCallMonitoring, 2, 2)
}

func terminateapp(watch_price int) {

	//fmt.Printf("terminate %d\n", watch_price)
	//time.Sleep(time.Second * 1)
}
